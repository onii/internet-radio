# Internet Radio
Python script to play and record from online radio streams. 

A simple alternative to 
[Streamripper](http://streamripper.sourceforge.net/)
which is included in most repos.

---

## Requirements:

+ `python3-requests`

+ `vlc` and `python-vlc` (to play streams).

---

## Examples:

*stations.ini*:

    ktbg = https://live.wostreaming.net/direct/publictv19-ktbgfmaac-imc1

*Play ktbg*:

    ./radio.py ktbg

*Play kbia with 30 minute sleep timer*:

    # radio.py <keyword> <sleep timer in minutes>
    ./radio.py kbia 30

*Record shows with cron*:

    # radio.py <keyword> <duration in minutes> <aac or mp3> <Keyword for filename (optional)>
    #
    # Revival
    0 10 * * 0 python3 ~/Radio/radio.py ktbg 120 aac Revival >/dev/null 2>&1
    # Will create: %m-%d-%y_%H_Revival.aac
    #
    # Snap Judgement
    0 12 * * 6 python3 ~/Radio/radio.py kcur 60 mp3 >/dev/null 2>&1
    # Will create: %m-%d-%y_%H_kcur.mp3
