#!/usr/bin/python3

import configparser, requests, signal, sys, time, vlc

def record(station, rectimer):
    container = ('.' + sys.argv[3])
    filename = (time.strftime("%m-%d-%y_%H_") + station)
    if len(sys.argv) == 5:
        filename = (time.strftime("%m-%d-%y_%H_") + sys.argv[4])
    filenameMP3 = (filename + '.mp3')
    filename = (filename + container)
    url = stations[(stations.index(sys.argv[1])+1)]
    class TimeoutException(Exception):
        pass
    def timeout_handler(signum, frame):
        raise TimeoutException
    print('Filename:', filename)
    addtime = (time.strftime("%I:%M %p", time.localtime(time.time() + rectimer)))
    print('Recording for:', int(rectimer/60),'minutes.', '(' + addtime + ')')
    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(rectimer)
    try:
        r = requests.get(url, stream=True, timeout=600)
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                f.write(chunk)
        return
    except:
        f.close()
# Convert recording to MP3 with ffmpeg
#
#        if sys.argv[3] != 'mp3':
#            print('Converting to MP3...')
#            subprocess.run(['ffmpeg', '-i', filename, filenameMP3])
#            print('Finished converting:',time.strftime("%I:%M %p"))
#            print('Removing',filename)
#            subprocess.run(['rm', filename])

        pass

def play(station, sleeptimer):
    if station not in stations:
        info()
        return
    url = stations[(stations.index(station)+1)]
    class TimeoutException(Exception):
        pass
    def timeout_handler(signum, frame):
        raise TimeoutException
    def playback():
        print('Press CTRL + C to exit.')
        player = vlc.MediaPlayer(url)
        player.play()
        current_state = player.get_state()
        # 6 means not running
        while current_state != 6:
            current_state = player.get_state()
            time.sleep(1)
    if sleeptimer:
        addtime = (time.strftime("%I:%M %p", time.localtime(time.time() + sleeptimer)))
        print('Will sleep in:', int(sleeptimer/60),'minutes.', '(' + addtime + ')')
        signal.signal(signal.SIGALRM, timeout_handler)
        signal.alarm(sleeptimer)
        playback()
    else:
        playback()

def info():
    infomsg = ('Usage: python3 radio.py <station>' + "\n" + 'Available Stations:')
    print(infomsg,"\n",stations[::2])

stations = ()
config = configparser.ConfigParser()
config.read(sys.path[0] + '/' + 'stations.ini')
for each_section in config.sections():
    for (each_key, each_val) in config.items(each_section):
        stations += (each_key, each_val)

if len(sys.argv) == 1:
    info();
    input = input('Input station: ')
    splitinput = input.split(" ")
    if len(splitinput) == 2:
        play(splitinput[0], (int(splitinput[1])*60))
    else:
        play(input, None)
if len(sys.argv) == 2:
    play(sys.argv[1], None)
if len(sys.argv) == 3:
    play(sys.argv[1], (int(sys.argv[2])*60))
if len(sys.argv) == 4 or len(sys.argv) == 5:
    record(sys.argv[1], (int(sys.argv[2])*60))
if len(sys.argv) > 5:
    info()
